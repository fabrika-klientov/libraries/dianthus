## Status

[![Latest Stable Version](https://poser.pugx.org/shadoll/dianthus/v/stable)](https://packagist.org/packages/shadoll/dianthus)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/dianthus/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/dianthus/commits/master)
[![coverage report](https://gitlab.com/fabrika-klientov/libraries/dianthus/badges/master/coverage.svg)](https://gitlab.com/fabrika-klientov/libraries/dianthus/commits/master)
[![License](https://poser.pugx.org/shadoll/dianthus/license)](https://packagist.org/packages/shadoll/dianthus)

Binotel telephony PHP API/hook library
[API Binotel](http://developers.binotel.ua/#integration-with-crm)

---


## Install

`composer require shadoll/dianthus`

---

## Использование

Библиотека в процессе разработки, заложен основной механизм (авторизация, клиент, Http клиент,
несколько моделей (которые будут пополнятся))

Клиент **\Dianthus\Client**

Первое что необходимо - создать клиент, от от которого вы сможете делать любые манипуляции

```php
$client = new \Dianthus\Client([
    'key' => '94378c-de44436',
    'secret' => 'c8a117-ce3431-173432-86c10e-0aa258ec',
]);

$httpClient = $client->getHttpClient();
```

Все методы моделей которые могут вернуть более одной сущности будут помещать их в коллекции 
`Dianthus\Core\Collection\Collection` которая в свою очередь наследуемая от 
`Illuminate\Support\Collection` и соответственно имеет широкий выбор различных методов для работы

**Модель `Dianthus\Models\Stats`**

Данный раздел используется для работы со статистикой звонков.

- `incoming-calls-for-period` Входящие звонки за период времени

```php
/**
 * @var \Dianthus\Core\Collection\Collection<\Dianthus\Models\Stats> $collect
 */
$collect = $client->stats->incomingCallsForPeriod(1551088402, 1575888402);

```

- `list-of-calls-per-day` Входящие и исходяшие звонки за день.

```php
/**
 * @var \Dianthus\Core\Collection\Collection<\Dianthus\Models\Stats> $collect
 */
$collect = $client->stats->listOfCallsPerDay();

```


---

### Работа с Hooks

Для выборки разных данных с веб Hooks есть `\Dianthus\Adapters\HookAdapter` в котором 
достаточное количество методов для выборки. Необходимо лишь передать данные с хука.

```php

$adapter = $client->hookAdapter($arrayOfHooks);
// или без клиента
$adapter = new \Dianthus\Adapters\HookAdapter($arrayOfHooks);

echo $adapter->getInternalNumber();
echo $adapter->getTrackingData();
// ...

```

