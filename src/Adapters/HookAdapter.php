<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Adapters
 * @category  Dianthus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Dianthus\Adapters;

use Dianthus\Client;
use Dianthus\Core\Helpers\Base;

/**
 * @property string $requestType
 * */
class HookAdapter extends Base
{
    /**
     * @var Client|null $client
     * */
    protected $client;

    /**
     * @var string[]
     * */
    protected $supportedRequestTypes = [
        'apiCallCompleted',
        'receivedTheCall',
        'answeredTheCall',
        'transferredTheCall',
        'hangupTheCall',
    ];

    /** constructor and init hook data
     * @param array $data
     * @param Client $client
     * @return void
     * */
    public function __construct(array $data = [], $client = null)
    {
        parent::__construct($data);
        $this->validate();
        $this->client = $client;
    }

    /**
     * @override
     * @return string
     * */
    public function getRequestType()
    {
        return $this->data['requestType'];
    }

    /**
     * @override
     * @return array
     * */
    public function getTags()
    {
        if (!empty($this->data['callDetails']['callTrackingData'])) {
            return ['callTracking'];
        }

        if (!empty($this->data['callDetails']['getCallData'])) {
            return ['getCall'];
        }

        return [];
    }

    /**
     * @override
     * @return bool
     * */
    public function isCallTracking()
    {
        return !empty($this->data['callDetails']['callTrackingData']);
    }

    /**
     * @override
     * @return bool
     * */
    public function isGetCall()
    {
        return !empty($this->data['callDetails']['getCallData']);
    }

    /**
     * @override
     * @return string|null
     * */
    public function getExternalNumber()
    {
        switch ($this->requestType) {

            case 'apiCallCompleted':
                return $this->data['callDetails']['externalNumber'];

            case 'answeredTheCall':
            case 'receivedTheCall':
                return $this->data['externalNumber'];
        }

        return null;
    }

    /**
     * @override
     * @return string|null
     * */
    public function getInternalNumber()
    {
        switch ($this->requestType) {

            case 'apiCallCompleted':
                return $this->data['callDetails']['internalNumber'];

            case 'answeredTheCall':
            case 'receivedTheCall':
            case 'transferredTheCall':
                return $this->data['internalNumber'];
        }

        return $this->getSourceNumber();
    }

    /**
     * @override
     * @return string|null
     * */
    public function getSourceNumber()
    {
        if (!empty($this->data['didNumber'])) {
            return $this->data['didNumber'];
        }

        if (isset($this->data['callDetails']['pbxNumberData']['number'])
            && !empty($this->data['callDetails']['pbxNumberData']['number'])) {
            return $this->data['callDetails']['pbxNumberData']['number'];
        }

        return null;
    }

    /**
     * @override
     * @param string|null $key
     * @return array|null
     * */
    public function getTrackingData(string $key = null)
    {
        if (isset($this->data['callDetails']['callTrackingData'])
            && !empty($this->data['callDetails']['callTrackingData'])) {
            $result = $this->getUtmData($this->data['callDetails']['callTrackingData'], $key == 'getTrackingData' ? null : $key);
            return empty($result) ? null : $result;
        }

        return null;
    }

    /**
     * @override
     * @param array $data
     * @param string|null $key
     * @return array|null
     * */
    protected function getUtmData($data, string $key = null)
    {
        $result = [];
        if ($data) {
            $result = array_reduce(array_keys($data), function ($result, $key) use ($data) {
                if ($key == 'id') {
                    return $result;
                }

                // if ($data[$key] == '(not set)') {
                //     return $result;
                // }

                $result[$key] = $data[$key];
                return $result;
            }, $result);

            if (isset($data['fullUrl']) && !empty($data['fullUrl'])) {
                $url = parse_url($data['fullUrl'], PHP_URL_QUERY);
                parse_str(empty($url) ? '' : $url, $query);
                if (!empty($query['gclid'])) {
                    $result['gclid'] = $query['gclid'];
                }
            }
        }

        if (!array_key_exists('utm_source', $result) || empty($data['utm_source'])) {
            if (!is_null($this->getSourceName())) {
                $result['utm_source'] = $this->getSourceName();
            }
        }

        return empty($result) ? null : (isset($key) ? ($result[$key] ?? null) : $result);
    }

    /**
     * @override
     * @return string|null
     * */
    public function getSourceName()
    {
        if (isset($this->data['callDetails']['pbxNumberData']['name'])
            && !empty($this->data['callDetails']['pbxNumberData']['name'])) {
            return $this->data['callDetails']['pbxNumberData']['name'];
        }

        return null;
    }

    /**
     * @override
     * @param string|null $key
     * @return array|null
     * */
    public function getCallData(string $key = null)
    {
        if (!empty($this->data['callDetails']['getCallData'])) {
            return $this->getUtmData($this->data['callDetails']['getCallData'], $key == 'getCallData' ? null : $key);
        }

        return null;
    }

    /**
     * @override
     * @return string
     * */
    public function getDisposition()
    {
        return !empty($this->data['callDetails']['disposition']) ? $this->data['callDetails']['disposition'] : '';
    }

    /**
     * @override
     * @return string
     * */
    public function getDisplayName()
    {
        return '';
    }

    /**
     * @override
     * @return string|null
     * */
    public function getSite()
    {
        return $this->data['callDetails']['getCallData']['domain']
            ?? $this->data['callDetails']['callTrackingData']['domain']
            ?? null;
    }

    /**
     * @override
     * @return string|int|null
     * */
    public function getId()
    {
        switch ($this->requestType) {

            case 'apiCallCompleted':
                return $this->data['callDetails']['generalCallID'];

            case 'answeredTheCall':
            case 'receivedTheCall':
            case 'transferredTheCall':
            case 'hangupTheCall':
                return $this->data['generalCallID'];
        }

        return null;
    }

    /**
     * @override
     * @return string|int|null
     * */
    public function getCompanyId()
    {
        return $this->data['companyID'] ?? $this->data['callDetails']['companyID'] ?? null;
    }

    /**
     * @override
     * @return bool
     * */
    public function isIncoming()
    {
        switch ($this->requestType) {

            case 'apiCallCompleted':
                return $this->data['callDetails']['callType'] == 0;

            case 'answeredTheCall':
            case 'receivedTheCall':
            case 'transferredTheCall':
            case 'hangupTheCall':
                return $this->data['callType'] == 0;
        }

        return false;
    }

    /**
     * @override
     * @return bool
     * */
    public function isAnswered()
    {
        return ($this->requestType == 'apiCallCompleted' && $this->data['callDetails']['disposition'] == 'ANSWER');
    }

    /**
     * @override
     * @return bool
     * */
    public function isCompleted()
    {
        return $this->requestType == 'apiCallCompleted';
    }

    /**
     * @override
     * @return string|int
     * */
    public function getStartTime()
    {
        return $this->data['callDetails']['startTime'] ?? time();
    }

    /**
     * @override
     * @return string|int
     * */
    public function getDuration()
    {
        return $this->data['callDetails']['billsec'] ?? 0;
    }

    /**
     * @override
     * @return string
     * */
    public function getRecordLink()
    {
        return $this->data['callDetails']['linkToCallRecordInMyBusiness'] ?? '';
    }

    /**
     * @override
     * @return void
     * @throws \Exception
     * */
    public function validate()
    {
        if (empty($this->data) || !is_array($this->data)) {
            throw new \Exception('Data is empty or not array');
        }

        $data = $this->data;

        if (empty($data['requestType'])) {
            throw new \Exception('[requestType] is NULL');
        }

        if (!in_array($data['requestType'], $this->supportedRequestTypes)) {
            throw new \Exception('[requestType] ' . $data['requestType']. ' not supported');
        }
    }
}
