<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Dianthus
 * @category  Dianthus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Dianthus;


use Dianthus\Core\Helpers\Instances;
use Dianthus\Core\Query\HttpClient;

class Client
{
    use Instances;

    /**
     * @var HttpClient $httpClient
     * */
    private $httpClient;

    /**
     * @param array $auth ['key' => string, 'secret' => string] (optional)
     * @return void
     * */
    public function __construct(array $auth = null)
    {
        if ($this->validateAuth($auth) && !empty($auth)) {
            $this->httpClient = new HttpClient($auth);
        }
    }

    /**
     * @return HttpClient|null
     * */
    public function getHttpClient()
    {
        return $this->httpClient ?? null;
    }

    /** client can request data
     * @return bool
     * */
    public function isActiveClient()
    {
        return isset($this->httpClient);
    }

    /** validator for auth data
     * @param array $auth
     * @return bool
     * */
    protected function validateAuth(array $auth = null)
    {
        if (empty($auth)) {
            return true;
        }

        if (isset($auth['key'], $auth['secret']) && is_string($auth['key']) && is_string($auth['secret'])) {
            return true;
        }

        return false;
    }
}