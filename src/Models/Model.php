<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Dianthus
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.09
 * @link      https://fabrika-klientov.ua
 */

namespace Dianthus\Models;


use Dianthus\Core\Build\Builder;
use Dianthus\Core\Helpers\Base;
use Dianthus\Core\Query\HttpClient;
use Illuminate\Support\Str;

abstract class Model extends Base
{
    /**
     * @var HttpClient $httpClient
     * */
    protected $httpClient;

    /**
     * @var Builder $builder
     * */
    protected $builder;

    /**
     * @var array $methods
     * */
    protected $methods = [];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod;

    public function __construct(HttpClient $client, array $data = [])
    {
        parent::__construct($data);
        $this->httpClient = $client;
        $this->builder = new Builder();
    }


    /** to request
     * @param Builder $builder
     * @return array|null
     * */
    protected function requestData(Builder $builder = null)
    {
        $builder = $builder ?? $this->builder;
        $options = $builder->getResult();

        return $this->httpClient->post($this->getEntry() . '/' . $this->getMethod(), ['json' => $options]);
    }

    /**
     * @param string $name
     * @param mixed $arguments
     * @return mixed
     * @throws \Exception
     * */
    public function __call($name, $arguments)
    {
        $method = Str::kebab($name);

        if (!array_key_exists($method, $this->methods)) {
            throw new \Exception('Method [' . $name . '] not found');
        }

        array_map(function ($key, $value) {
            $this->builder->{$key}($value);
            return null;
        }, $this->methods[$method], $arguments);

        $this->currentMethod = $method . '.json';

        return $this->requestData();
    }

    /** get active method
     * @return string
     * */
    protected function getMethod(): string
    {
        return $this->currentMethod;
    }

    /** small entry partition
     * @return string
     * */
    protected function getEntry()
    {
        return Str::lower(last(explode('\\', get_class($this))));
    }

}
