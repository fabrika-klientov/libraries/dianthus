<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Models
 * @category  Dianthus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.10
 * @link      https://fabrika-klientov.ua
 */

namespace Dianthus\Models;

use Dianthus\Core\Collection\Collection;

/**
 * @method Collection incomingCallsForPeriod($startTime, $stopTime)
 * @method Collection listOfCallsPerDay()
 * */
final class Stats extends Model
{
    /**
     * @var array $methods
     * */
    protected $methods = [
        'incoming-calls-for-period' => ['startTime', 'stopTime'],
        'list-of-calls-per-day' => [],
    ];

    /**
     * @param string $name
     * @param mixed $arguments
     * @return mixed
     * @throws \Exception
     * */
    public function __call($name, $arguments)
    {
        $result = parent::__call($name, $arguments);

        if (isset($result['callDetails'])) {
            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $result['callDetails']));
        }

        return null;
    }

}