<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Dianthus
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.09
 * @link      https://fabrika-klientov.ua
 */

namespace Dianthus\Core\Query;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class HttpClient
{
    /**
     * @var Client $client
     * */
    private $client;

    /**
     * @var array $authData
     * */
    private $authData;

    /**
     * @var string $LINK
     * */
    private static $LINK = 'https://api.binotel.com/api/3.0/';

    /**
     * @param array $auth
     * @return void
     * */
    public function __construct(array $auth)
    {
        $this->authData = $auth;
        $this->client = new Client([
            'base_uri' => self::$LINK,
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);
    }

    /** api POST
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws \Exception
     * */
    public function post($link, $options)
    {
        return $this->request('POST', $link, $options);
    }

    /** auth data
     * @return array
     * */
    public function getAuth()
    {
        return $this->authData;
    }

    /** full url
     * @param string $link
     * @return string
     * */
    public function getURL(string $link = '')
    {
        return self::$LINK . $link;
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws \Exception
     * */
    protected function request(string $method, string $link, array $options)
    {
        try {
            $this->injectSignature($options['json']);

            $result = json_decode($this->client->request($method, $link, $options)->getBody()->getContents(), true);

            if (!empty($result)) {
                return $result;
            }
        } catch (ClientException $clientException) {
            throw new \Exception('Request returned error. ' . $clientException->getMessage(), $clientException->getCode());
        } catch (\Exception $exception) {
            throw new \Exception('Request returned error. ' . $exception->getMessage());
        }

        return null;
    }

    /** injecting auth data
     * @param array $params
     * @return void
     * */
    protected function injectSignature(array &$params)
    {
        ksort($params);
        $params['signature'] = md5($this->authData['secret'] . json_encode($params));
        $params['key'] = $this->authData['key'];
    }
}