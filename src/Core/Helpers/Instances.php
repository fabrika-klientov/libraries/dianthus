<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Dianthus
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.09
 * @link      https://fabrika-klientov.ua
 */

namespace Dianthus\Core\Helpers;


use Dianthus\Adapters\HookAdapter;
use Illuminate\Support\Str;

/**
 * @property-read \Dianthus\Models\Stats $stats
 *
 * @method  \Dianthus\Adapters\HookAdapter hookAdapter(array $data)
 * */
trait Instances
{

    /** getter magic instances
     * @param string $name
     * @return \Dianthus\Models\Model|null
     * */
    public function __get($name)
    {
        switch ($name) {
            case 'stats':
                $class = 'Dianthus\\Models\\' . Str::ucfirst($name);
                return new $class($this->httpClient);
        }

        return null;
    }

    /** call magic instances
     * @param string $name
     * @param mixed $arguments
     * @return mixed
     * */
    public function __call($name, $arguments)
    {
        switch ($name) {
            case 'hookAdapter':
                return new HookAdapter(head($arguments), $this);
        }

        return null;
    }
}