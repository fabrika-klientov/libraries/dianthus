<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Dianthus
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.12.09
 * @link      https://fabrika-klientov.ua
 */

namespace Dianthus\Core\Collection;


use Illuminate\Support\Collection as BaseCollection;

class Collection extends BaseCollection
{

}