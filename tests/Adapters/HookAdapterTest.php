<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 11:46
 */

namespace Tests\Adapters;

use Dianthus\Adapters\HookAdapter;
use Dianthus\Client;
use PHPUnit\Framework\TestCase;

class HookAdapterTest extends TestCase
{
    /**
     * @var HookAdapter $adapter
     * */
    protected $adapter;
    protected $hook = '{"hash":"205766926e923bb8f2ae6fbcfdcbbce3200663c6","requestType":"apiCallCompleted","attemptsCounter":"1","callDetails":{"companyID":"26446","generalCallID":"1626082880","callID":"1626082883","startTime":"1575897836","callType":"1","internalNumber":"908","internalAdditionalData":"","externalNumber":"0509406186","waitsec":"12","billsec":"41","disposition":"ANSWER","isNewCall":"0","customerData":{"id":"29948973","name":"Олабин Олексій СОЛАНА ЛОГІСТИКА ТОВ"},"employeeData":{"name":"Луц Алеся Пожарка","email":"luts.alesya@euroservis.com.ua"},"pbxNumberData":{"number":"0503389676"},"historyData":[{"waitsec":"12","billsec":"41","disposition":"ANSWER","pbxNumberData":{"number":"0503389676"}}],"linkToCallRecordOverlayInMyBusiness":"https://my.binotel.ua/?module=history&subject=0509406186&sacte=ovl-link-pb-1626082880","linkToCallRecordInMyBusiness":"https://my.binotel.ua/?module=cdrs&action=generateFile&fileName=1626082883.mp3&callDate=2019-09-12_15:23&customerNumber=0509406186&callType=outgoing"}}';

    public function test__construct()
    {
        $adapter = new HookAdapter(json_decode($this->hook, true));

        $this->assertInstanceOf(HookAdapter::class, $adapter);

        $client = new Client([
            'key' => '94378c-de17333',
            'secret' => 'c8a117-ce0c31-17bb92-22210e-0aa258ec',
        ]);

        $adapter = $client->hookAdapter(json_decode($this->hook, true));

        $this->assertInstanceOf(HookAdapter::class, $adapter);

        $adapter = new HookAdapter(json_decode($this->hook, true), $client);

        $this->assertInstanceOf(HookAdapter::class, $adapter);

        $hook = json_decode($this->hook, true);

        unset($hook['requestType']);

        $this->expectException(\Exception::class);

        new HookAdapter($hook);
    }

    public function testGetRequestType()
    {
        $this->assertEquals('apiCallCompleted', $this->adapter->getRequestType());
    }

    public function testGetTags()
    {
        $this->assertEmpty($this->adapter->getTags());
    }

    public function testIsCallTracking()
    {
        $this->assertFalse($this->adapter->isCallTracking());
    }

    public function testIsGetCall()
    {
        $this->assertFalse($this->adapter->isGetCall());
    }

    public function testGetExternalNumber()
    {
        $this->assertEquals('0509406186', $this->adapter->getExternalNumber());
    }

    public function testGetInternalNumber()
    {
        $this->assertEquals('908', $this->adapter->getInternalNumber());
    }

    public function testGetSourceNumber()
    {
        $this->assertEquals('0503389676', $this->adapter->getSourceNumber());
    }

    public function testGetTrackingData()
    {
        $this->assertNull($this->adapter->getTrackingData());
    }

    public function testGetSourceName()
    {
        $this->assertNull($this->adapter->getSourceName());
    }

    public function testGetCallData()
    {
        $this->assertNull($this->adapter->getCallData());
    }

    public function testGetDisposition()
    {
        $this->assertEquals('ANSWER', $this->adapter->getDisposition());
    }

    public function testGetDisplayName()
    {
        $this->assertEmpty($this->adapter->getDisplayName());
        $this->assertEquals('', $this->adapter->getDisplayName());
    }

    public function testGetSite()
    {
        $this->assertNull($this->adapter->getSite());
    }

    public function testGetId()
    {
        $this->assertEquals(1626082880, $this->adapter->getId());
    }

    public function testGetCompanyId()
    {
        $this->assertEquals(26446, $this->adapter->getCompanyId());
    }

    public function testIsIncoming()
    {
        $this->assertFalse($this->adapter->isIncoming());
    }

    public function testIsAnswered()
    {
        $this->assertTrue($this->adapter->isAnswered());
    }

    public function testIsCompleted()
    {
        $this->assertTrue($this->adapter->isCompleted());
    }

    public function testGetStartTime()
    {
        $this->assertEquals(1575897836, $this->adapter->getStartTime());
    }

    public function testGetDuration()
    {
        $this->assertEquals(41, $this->adapter->getDuration());
    }

    public function testGetRecordLink()
    {
        $this->assertIsString($this->adapter->getRecordLink());
        $this->assertEquals(
            'https://my.binotel.ua/?module=cdrs&action=generateFile&fileName=1626082883.mp3&callDate=2019-09-12_15:23&customerNumber=0509406186&callType=outgoing',
            $this->adapter->getRecordLink()
        );
    }

    protected function setUp(): void
    {
        $this->adapter = new HookAdapter(json_decode($this->hook, true));
    }

    protected function tearDown(): void
    {
        $this->adapter = null;
    }
}
