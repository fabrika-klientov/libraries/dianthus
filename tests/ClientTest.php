<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 09.12.19
 * Time: 18:03
 */

namespace Tests;

use Dianthus\Client;
use Dianthus\Core\Query\HttpClient;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{

    public function test__construct()
    {
        $client = new Client();
        $this->assertInstanceOf(Client::class, $client);

        $client = new Client([
            'key' => '94378c-de17c86',
            'secret' => 'c8a117-ce0c31-17bb92-86c10e-0aa258ec',
        ]);

        $this->assertInstanceOf(Client::class, $client);
    }

    public function testGetHttpClient()
    {

        $client = new Client([
            'key' => '94378c-de17c86',
            'secret' => 'c8a117-ce0c31-17bb92-86c10e-0aa258ec',
        ]);

        $this->assertInstanceOf(HttpClient::class, $client->getHttpClient());

        $client = new Client([
            'secret' => 'c8a117-ce0c31-17bb92-86c10e-0aa258ec',
        ]);

        $this->assertNull($client->getHttpClient());

        $client = new Client([
            'key' => '94378c-de17c86',
        ]);

        $this->assertNull($client->getHttpClient());

        $client = new Client([]);

        $this->assertNull($client->getHttpClient());

        $client = new Client();

        $this->assertNull($client->getHttpClient());
    }
}
