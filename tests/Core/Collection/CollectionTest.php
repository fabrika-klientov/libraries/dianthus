<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 18:21
 */

namespace Tests\Core\Collection;

use Dianthus\Client;
use Dianthus\Core\Collection\Collection;
use Dianthus\Models\Stats;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{

    public function test__construct()
    {
        $client = new Client([
            'key' => '94378c-de17333',
            'secret' => 'c8a117-ce0c31-17bb92-22210e-0aa258ec',
        ]);

        $collect = new Collection(array_map(function ($one) use ($client) {
            return new Stats($client->getHttpClient(), ['id' => $one]);
        }, [1, 2, 3, 4, 5]));

        $this->assertInstanceOf(Collection::class, $collect);

        $this->assertCount(5, $collect);

        $this->assertLessThan(6, $collect->shuffle()->first()->id);
    }
}
