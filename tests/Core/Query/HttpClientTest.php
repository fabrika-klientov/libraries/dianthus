<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 18:16
 */

namespace Tests\Core\Query;

use Dianthus\Core\Query\HttpClient;
use PHPUnit\Framework\TestCase;

class HttpClientTest extends TestCase
{

    /**
     * @var HttpClient $client
     * */
    protected $client;

    public function test__construct()
    {
        $client = new HttpClient([
            'key' => '94378c-de17333',
            'secret' => 'c8a117-ce0c31-17bb92-22210e-0aa258ec',
        ]);

        $this->assertInstanceOf(HttpClient::class, $client);

    }

    public function testPost()
    {
        // is request
        $this->assertTrue(true);
    }

    public function testGetAuth()
    {
        // is request
        $this->assertTrue(true);
    }

    public function testGetURL()
    {
        $this->assertEquals('https://api.binotel.com/api/3.0/', $this->client->getURL());
        $this->assertEquals('https://api.binotel.com/api/3.0/entry', $this->client->getURL('entry'));
    }

    protected function setUp(): void
    {
        $this->client = new HttpClient([
            'key' => '94378c-de17333',
            'secret' => 'c8a117-ce0c31-17bb92-22210e-0aa258ec',
        ]);
    }
}
