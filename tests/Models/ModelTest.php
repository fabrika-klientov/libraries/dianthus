<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 10.12.19
 * Time: 18:09
 */

namespace Tests\Models;

use Dianthus\Client;
use Dianthus\Models\Model;
use Dianthus\Models\Stats;
use PHPUnit\Framework\TestCase;

class ModelTest extends TestCase
{

    public function test__construct()
    {
        $client = new Client([
            'key' => '94378c-de17333',
            'secret' => 'c8a117-ce0c31-17bb92-22210e-0aa258ec',
        ]);

        $model = new Stats($client->getHttpClient());

        $this->assertInstanceOf(Model::class, $model);
        $this->assertInstanceOf(Stats::class, $model);

        $model = new Stats($client->getHttpClient(), ['any' => 'data']);

        $this->assertEquals('data', $model->any);
    }

    public function test__call()
    {
        // magic
        $this->assertTrue(true);
    }
}
